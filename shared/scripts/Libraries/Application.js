source(findFile("scripts", "Libraries/Global.js"))
source(findFile("scripts", "Libraries/Constants.js"))
source(findFile("scripts", "Libraries/Perf.js"))

function Application()
{
    
    var objGlobal = new GlobalVariables();
    var objConstants = new Constants();
    var objPerf = new Perf();
    var productQuantityGlobal;
   
    //var Global = new GlobalVariables();
    
    //To Start The XTuple Application 
    this.start = function start(sAutName)
    {
        objPerf.startPerf("Transaction Name: Launch xTuple Application");
        
        startApplication(sAutName);
        
        objPerf.stopPerf("Transaction Name: Launch xTuple Application");
    }
    
    //Login Home Page 
    function updateLoginInfo()
    {
        var dataset = testData.dataset("LoginDetails.tsv");
        
        for (var record in dataset) 
        {
            objGlobal.LoginUserName = testData.field(dataset[record], "LoginUserName");
            objGlobal.LoginPassword = testData.field(dataset[record], "LoginPassword");
            objGlobal.ServerName = testData.field(dataset[record], "ServerName");
            objGlobal.PortNo = testData.field(dataset[record], "PortNo");
            objGlobal.DataBaseName = testData.field(dataset[record], "DataBaseName");
        }
    }
    /**** un Checking the check box*****/
    function UnCheckingcheckbox(Checkbox_object)
    {
    var unbCheck =  waitForObjectExists(Checkbox_object).checked;    
    test.log(unbCheck);
    
    if(unbCheck)           
      {
          clickButton(waitForObject(Checkbox_object));
      }
    }
    /***** checking the check box*****/
    function Checkingcheckbox(Checkbox_object1)
    {
    var bCheck =  waitForObjectExists(Checkbox_object1).checked;    
    test.log(bCheck);
    test.log("checking the check box");
    
    if(bCheck== false)           
      {
          clickButton(waitForObject(Checkbox_object1));
      }
    }
    //enter Login Details & input should come from Database 
    
        this.login = function loginToApplication()
       {
        updateLoginInfo();
        
        objPerf.startPerf("Transaction Name: Login to Application");
        
        waitForObject(":frmLogin", objConstants.WaitForObjectTimeOut);
        
        type(waitForObject(":txtLoginUserName"), objGlobal.LoginUserName);
        type(waitForObject(":txtLoginPassword"), objGlobal.LoginPassword);
        mouseClick(waitForObject(":txtServerName"));
        type(waitForObject(":txtServerName"),"<Ctrl+A>");
        type(waitForObject(":txtServerName"), objGlobal.ServerName);
        mouseClick(waitForObject(":txtPortNumber"));
        doubleClick(waitForObject(":txtPortNumber"));
        type(waitForObject(":txtPortNumber"), objGlobal.PortNo);
        mouseClick(waitForObject(":txtDatabase"));
        doubleClick(waitForObject(":txtDatabase"));
        type(waitForObject(":cmbDatabase"), objGlobal.DataBaseName);
        clickButton(waitForObject(":btnLogin"));
   
        waitForObject(":SplashScreen");
        //waitForObject(":frmHome");

        
        objPerf.stopPerf("Transaction Name: Login to Application");
        
        return true;
    }
  
    //Navigate to QMenu 
    
    this.navigateToMenu = function navigateToMenu(sMenuPath)
    {   
        var arrMenuPath = sMenuPath.split("|");
        
        activateItem(waitForObjectItem(":MainMenuBar", arrMenuPath[0]));
        
        for (var iLoopCounter = 1; iLoopCounter < arrMenuPath.length - 1; iLoopCounter++) 
        {
            activateItem(waitForObjectItem(":MenuItem", arrMenuPath[iLoopCounter]));
        }
      
      // Select 'Customer'option from 'sales' tab on the QMenu window  
        
        if(sMenuPath.includes("Customer"))
        {
            activateItem(waitForObjectItem(":menu.sales.customer_QMenu",arrMenuPath[arrMenuPath.length - 1]))
        }
    }

           //Navgate to SalesCustomer Screen

        this.navigateToSalesCustomersScreen = function navigateToSalesCustomersScreen()
    {
        objPerf.startPerf("Transaction Name: Navigate to Sales|Customer|List");
        
        activateItem(waitForObjectItem(":MainMenuBar", "Sales"));
        activateItem(waitForObjectItem(":MenuItem", "Customer"));
        activateItem(waitForObjectItem(":menu.sales.customer_QMenu","List..."));
        
        waitForObject(":frmCustomers");
        
        objPerf.stopPerf("Transaction Name: Navigate to Sales|Customer|List");
        
        return true;
    }
    //Performing actions on 'Customer' Window 
    
        this.disableDefaultQueryOnStart = function disableQueryOnStart(menuControl, controlToUnCheck)
        {
        objPerf.startPerf("Transaction Name: Disable Query on Start");
        
        sendEvent("QMouseEvent", waitForObject(controlToUnCheck), QEvent.MouseButtonPress, 50, 12, Qt.LeftButton, 1, 0);
        
        //var obj = waitForObject(controlToUnCheck);
       // var Prp = object.properties(obj);
        
      //  var bChecked = Prp["checked"];
        var bChecking =  waitForObjectExists(":customers._queryonstart_XCheckBox").checked;    
               
             
              if(bChecking)           
                {
            sendEvent("QMouseEvent", waitForObject(controlToUnCheck), QEvent.MouseButtonPress, 50, 12, Qt.LeftButton, 1, 0);
            activateItem(waitForObjectItem(menuControl, "Query on start"));
        }
        
        objPerf.stopPerf("Transaction Name: Disable Query on Start");
    }
     
    //Retrieving customer details 
    this.searchAndSelectCustomer = function searchAndSelectCustomer(sFilterCriteria, sFilterValue)
    {
        objPerf.startPerf("Transaction Name: Search for Customer");
        
       clickButton(waitForObject(":_filterGroup._addFilterRow_QToolButton"));
        
        //clickButton(waitforObject(":customers._moreBtn_QToolButton"));
        //clickButton(waitforObject(":customers._moreBtn_QToolButton"));
        mouseClick(waitForObjectItem(":_filterGroup.xcomboBox1_XComboBox", sFilterCriteria));
        
        type(waitForObject(":_filterGroup.widget1_QLineEdit"), sFilterValue);
        
        clickButton(waitForObject(":customers._queryBtn_QToolButton"));
        
        waitForObjectItem(":_list_XTreeWidget", sFilterValue);
        
        objPerf.stopPerf("Transaction Name: Search for Customer");
        
        doubleClickItem(":_list_XTreeWidget", "" + sFilterValue, 10, 10, 0, Qt.LeftButton);
        
        objPerf.startPerf("Transaction Name: Wait for Customer Details");
        
        waitForObjectExists(":customer.qt_tabwidget_tabbar_QTabBar",objConstants.WaitForObjectTimeOut);
        
        objPerf.stopPerf("Transaction Name: Wait for Customer Details");
      
    }
    
   
    
    
    //Click on Sales tab from Customer Window
    this.openNewSalesOrderWindow = function openNewSalesOrderWindow()
    {
        clickTab(waitForObject(":customer.qt_tabwidget_tabbar_QTabBar_2"), "Sales");
        
        objPerf.startPerf("Transaction Name: Open New Sales Order Window");
        
        clickButton(waitForObject(":_salesTab._ordersButton_QRadioButton"));

        clickButton(waitForObject(":_salesStack._newBtn_QToolButton"));
        test.compare(waitForObjectExists(":salesOrder new_salesOrder").windowTitle, "Sales Order");
        mouseClick(waitForObject(":salesOrder new_salesOrder"), 356, 9, 0, Qt.LeftButton);
        waitForObject(":salesOrder new._salesOrderInformation_QTabWidget");
        
        objPerf.stopPerf("Transaction Name: Open New Sales Order Window");
     clickTab(waitForObject(":salesOrder new._salesOrderInformation_QTabWidget"), "Line Items");
        
        clickButton(waitForObject(":_lineItemsPage._new_QPushButton"));
    }
    
    //******************NAvigated to SalesOrder Window*************************// 
    
        this.addNewLineItem = function addNewLineItem(ProductName, ProductQuantity, DiscountPercentage,bReserve, bRefreshOrder)
        {
            productQuantityGlobal=ProductQuantity;
        objPerf.startPerf("Transaction Name: Add New Line Item");
        var objApp = new Application();
        
        
        type(waitForObject(":_itemGroup.ItemLineEdit_ItemLineEdit"), ProductName);
        
        objPerf.startPerf("Transaction Name: Search Product");
      
        objPerf.stopPerf("Transaction Name: Search Product");
        
        type(waitForObject(":_qtyOrdered_XLineEdit"), ProductQuantity); 

        mouseClick(waitForObject(":_amountGroup.XLineEdit_XLineEdit_5"), 69, 8, 0, Qt.LeftButton);
        mouseDrag(waitForObject(":_amountGroup._discountFromListPrice_XLineEdit"), 80, 8, 81, 0, 1, Qt.LeftButton);    
        
        var chkReserve = waitForObject(":salesOrderItem._reserveOnSave_XCheckBox");
        var Prp = object.properties(chkReserve);
        var bChecked = Prp["checked"];
        
        if(bReserve)
        {
            if(bChecked == false)
            {
                clickButton(waitForObject(":salesOrderItem._reserveOnSave_XCheckBox"));
            }
        }
        
        if(!bReserve)
        {
            if(bChecked == true)
            {
                clickButton(waitForObject(":salesOrderItem._reserveOnSave_XCheckBox")); 
            }
        }
        
        var chkRefresh = waitForObject(":salesOrderItem._orderRefresh_XCheckBox");
        var RefreshPrp = object.properties(chkRefresh);
        bChecked = RefreshPrp["checked"];
        
        if(bRefreshOrder)
        {
            if(bChecked == false)
            {
                clickButton(waitForObject(":salesOrderItem._orderRefresh_XCheckBox"));
            }
        }
        
        if(!bRefreshOrder)
        {
            if(bChecked == true)
            {
                clickButton(waitForObject(":salesOrderItem._orderRefresh_XCheckBox")); 
            }
        }
        
        //clickButton(waitForObject(":salesOrderItem._save_QPushButton"));
        //clickButton(waitForObject(":salesOrderItem._closeBtn"));
        
        
        
       // clickButton(waitForObject(":salesOrder new._save_QPushButton"));
        
       // objPerf.stopPerf("Transaction Name: Add New Line Item");
        
        
    }
        
    
    // Geting order Details//
    
    
    
    
    
    
    
    
    function verifyLineItemValue(objLineItem, sLineItemExpValue)
    {
        var obj;
        try
        {
            obj = waitForObjectExists(objLineItem);
        }
        catch(Exception)
        {
            return false;
        }
        
           
        var sActualValue = obj.text;
             sActualValue=removeCommas(sActualValue);    
             objPerf.startPerf(sActualValue);
              sLineItemExpValue=removeCommas(sLineItemExpValue);          
            test.log("Actual Value ", sActualValue);            
           test.log("Expected value ", sLineItemExpValue);
        
           if(sActualValue == sLineItemExpValue) return true;
        else
        {
            objGlobal.gErrMsg = "Expected Value: " + sLineItemExpValue + "\n Actual Value Displayed: " + sActualValue;
            return false;
        }    
    }

    this.getOrderDetailsColNo = function getOrderDetailsColNo(sColumnName)
    {
        switch (sColumnName) 
        {
            case "Product Quantity":
                return 1;
            case "Product Name":
                return 0;
            case "ProductPrice":
                return 2;
            case "ProductExtendedPrice":
                return 3;
            case "ProductCustPrice":
                return 4;
            case "Reserved":
                return 5;
            case "SubTotal":
                return 6;
            case "Total":
                return 7;
            case "Tax":
                return 8;
            case "Discount Percentage":
                return 9;
            case "ListPrice":
                return 10;
            case "DiscountTwoZeros":
            return 11;
        default:
            break;
        }
    }
    
    this.getCustomerDetailsColNo = function getCustomerDetailsColNo(sColumnName)
    {
        switch (sColumnName) 
        {
            case "Value":
                return 1;
            case "Filter Criteria":
                return 0;
           
        default:
            break;
        }
    }
   
    
    
    
    
    
    // ************************************Navigating to pakinglist batch******************************************//
    this.navigateToPackingListBatch = function navigateToPackingListBatch()
    {
        objPerf.startPerf("Transaction Name: Navigate to Inventory|Shipping|Form|pakingListBatch");
        
        activateItem(waitForObjectItem(":MainMenuBar", "Inventory"));
        activateItem(waitForObjectItem(":MenuItem", "Shipping"));
        activateItem(waitForObjectItem(":menu.im.shipping_QMenu", "Forms"));
        activateItem(waitForObjectItem(":menu.im.shippingforms_QMenu", "Packing List Batch..."));

        
        objPerf.stopPerf("Transaction Name: Navigate to Inventory|Shipping|Form|pakingListbatch");
        
        return true;
    }    
    
    
    
    function selectMaxQauntity(){
        var dsOrderDetails = testData.dataset("OrderDetails.tsv");
        sleep(20000);
        var objApp = new Application();
        var sWidgetName = ":_itemloc_XTreeWidget";
        
        waitForObject(sWidgetName);
        var obj_QTableWidget = findObject(sWidgetName);
        var obj_QAbstratItemModel = obj_QTableWidget.model();  
        var iRowCount = obj_QAbstratItemModel.rowCount();
        var iColumnCount = obj_QAbstratItemModel.columnCount();

        test.log("The table has " , iRowCount); 
        test.log("The table has " , iColumnCount);
        var numberOfLots = new Array(iRowCount);
        var dateOfExp = new Array(iRowCount);
        var dateOfExpActual = new Array(iRowCount);
        
    var i = 0;
    while (i < iRowCount)
    {
        test.log("We are in row " , i);
        
        numberOfLots[i]= getTextValue("{column='6' container=':_itemloc_XTreeWidget' row='"+i+"' type='QModelIndex'}");
        dateOfExp[i]=getTextValue("{column='5' container=':_itemloc_XTreeWidget' row='"+i+"' type='QModelIndex'}");
        // Lets loop through each column in each row
        test.log("expiration date " , dateOfExp[i]);
        dateOfExpActual[i] = dateOfExp[i];
        dateOfExp[i]=getNumberOfDays(dateOfExp[i]);
        numberOfLots[i]=parseInt(removeCommas(numberOfLots[i]));
        test.log("number of lots " , numberOfLots[i]);
        test.log("days " , dateOfExp[i]);
        i++;
    }
    test.log(numberOfLots.length);
    var maxNumLotsIndex = numberOfLots.indexOf(Math.max.apply(Math, numberOfLots));
    var earlistExpDateIndex = dateOfExp.indexOf(Math.min.apply(Math, dateOfExp));
    maxNumLotsIndex=Math.abs(maxNumLotsIndex);
    earlistExpDateIndex=Math.abs(earlistExpDateIndex);
    test.log("index for max number of slots " , maxNumLotsIndex);
    test.log("index for earliest expiration date " , earlistExpDateIndex);
    var lotsForDateMax=parseInt(numberOfLots[earlistExpDateIndex]);
    test.log(lotsForDateMax);
    test.log(productQuantityGlobal);  
    
       if(lotsForDateMax>productQuantityGlobal){
           waitForObjectItem(":_itemloc_XTreeWidget", dateOfExpActual[earlistExpDateIndex]);
           clickItem(":_itemloc_XTreeWidget", dateOfExpActual[earlistExpDateIndex], 29, 8, 0, Qt.LeftButton);

       }
    else{
        waitForObjectItem(":_itemloc_XTreeWidget", dateOfExpActual[maxNumLotsIndex]);
        clickItem(":_itemloc_XTreeWidget", dateOfExpActual[maxNumLotsIndex], 29, 8, 0, Qt.LeftButton);

        
    }
   
    }
    
    function getNumberOfDays(date1){
        try{
            if (date1=="N/A"){
                return 9999999999999;
            }
        date1 = date1.split('/');
        date1[2]="20"+date1[2];
        // Now we convert the array to a Date object, which has several helpful methods
        date1 = new Date(date1[2], date1[0], date1[1]);
 
        // We use the getTime() method and get the unixtime (in milliseconds, but we want seconds, therefore we divide it through 1000)
        date1_unixtime = parseInt(date1.getTime() / 1000);
 
        // in Hours
        var timeDifferenceInHours = date1_unixtime / 60 / 60;
 
        // and finaly, in days :)
        var timeDifferenceInDays = timeDifferenceInHours  / 24;
 
        return timeDifferenceInDays;
        }           
        catch(Exception)        
       {           
            return false;           
       } 
    }
   
    function removeCommas(str) {
        var textValue=str.toString();
        while (textValue.search(",") >= 0) {
            textValue = (textValue + "").replace(',', '');
        }
        return textValue;
    }
   
    
    function sleep(milliseconds){
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
          if ((new Date().getTime() - start) > milliseconds){
            break;
          }
        }
     }
    
    function distributeAllLineitems(){
        var sWidgetName = ":_frame._soitem_XTreeWidget";
        
        waitForObject(sWidgetName);
        var obj_QTableWidget = findObject(sWidgetName);
        var obj_QAbstratItemModel = obj_QTableWidget.model();  
        var iRowCount = obj_QAbstratItemModel.rowCount();
         var iColumnCount = obj_QAbstratItemModel.columnCount();

        test.log("The table has " + iRowCount + " row(s)"); 
        test.log("The table has " + iColumnCount + " column(s)");
        var i=1;
       // while (i <= iRowCount)
        clickButton(waitForObject(":_frame._issueAll_QPushButton"));
            for(i=1;i<=iRowCount;i++)
        {
            test.log("We are in row " + i);
            // Lets loop through each column in each row
           
            selectMaxQauntity();
           
            clickButton(waitForObject(":_frame._distribute_QPushButton"));
            clickButton(waitForObject(":distributeToLocation._distribute_QPushButton"));
            clickButton(waitForObject(":distributeInventory._post_QPushButton"));
            
            
          
        }
    }
    //************************************* Navigating to issue Shipping**********************************************//
    this.navigateToIssueShipping = function navigateToIssueShipping(OrderNumber)
    {
        objPerf.startPerf("Transaction Name: Navigate to Inventory|Shipping|issueShipping");
        activateItem(waitForObjectItem(":MainMenuBar", "Inventory"));
        activateItem(waitForObjectItem(":MenuItem", "Shipping"));
        activateItem(waitForObjectItem(":menu.im.shipping_QMenu", "Issue to Shipping..."));
        
        type(waitForObject(":_QTreeView"), OrderNumber);
     
        mouseClick(waitForObject(":_frame._soitem_XTreeWidget"), 299, 154, 0, Qt.LeftButton);
        waitForObjectItem(":_frame._soitem_XTreeWidget", "0\\.000_2");
        clickItem(":_frame._soitem_XTreeWidget", "0\\.000_2", 1, 9, 0, Qt.LeftButton);
        distributeAllLineitems();
         
        clickButton(waitForObject(":issueToShipping._ship_QPushButton"));
        Checkingcheckbox(":_optionGroup._pr_XCheckBox1");
        Checkingcheckbox(":_optionGroup._pt_XCheckBox2");
        UnCheckingcheckbox(":_optionGroup._print_XCheckBox");
       
        clickButton(waitForObject(":shipOrder._ship_QPushButton"));  
        
            return true;
            }
        
        /**** getting invoice number*****/
        this.getinvoiceNumber=function getinvoiceNumber()
        {
           
            clickButton(waitForObject(":printMulticopyDocument._post_XCheckBox"));
           var invoiceNumber=getTextValue(":_optionsFrame._invoiceNum_QLabel");
        test.log(invoiceNumber);
        clickButton(waitForObject(":printMulticopyDocument._close_QPushButton"));
        sendEvent("QCloseEvent", waitForObject(":issueToShipping_issueToShipping"));
        objPerf.stopPerf("Transaction Name: Navigate to Inventory|Shipping|issueShipping");
        
        return invoiceNumber;
        
      
    } 
  //********Verifying invoice under Accounting in SalesOrder*************
  
  
    this.verifyAlltheLineItemValuesOnOrderPage = function verifyAlltheLineItemValuesOnOrderPage(testCaseName)
    {
        var vStatus = false;
        var objApp = new Application();
         
        var dsOrderDetails = testData.dataset("OrderDetails.tsv");
        
        vStatus = verifyLineItemValue(":_amountGroup.XLineEdit_XLineEdit12",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ProductCustPrice")],testCaseName));
        if(vStatus == false) test.fail("Verify line item 'ProductPrice'",objGlobal.gErrMsg);
        else test.pass("Verify line item 'ProductPrice'","'ProductPrice' value is displayed as expected");
        
        vStatus = verifyLineItemValue(":_amountGroup._discountFromListPrice_XLineEdit",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Discount Percentage")],testCaseName));          
        if(vStatus == false) test.fail("Verify line item 'Discount Percentage'",objGlobal.gErrMsg);             
        else test.pass("Verify line item 'Discount Percentage'","'Discount Percentage' value is displayed as expected");  

                     
        vStatus = verifyLineItemValue(":_amountGroup.XLineEdit_XLineEdit_2",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ListPrice")],testCaseName));
        if(vStatus == false) test.fail("Verify line item 'ListPrice'",objGlobal.gErrMsg);
        else test.pass("Verify line item 'ListPrice'","'ListPrice' value is displayed as expected");
        
        vStatus = verifyLineItemValue(":_amountGroup.XLineEdit_XLineEdit_3",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ProductExtendedPrice")],testCaseName));
        if(vStatus == false) test.fail("Verify line item 'ProductExtendedPrice'",objGlobal.gErrMsg);
        else test.pass("Verify line item 'ProductExtendedPrice'","'ProductExtendedPrice' value is displayed as expected");
        
        vStatus = verifyLineItemValue(":_amountGroup.XLineEdit_XLineEdit_4",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ProductCustPrice")],testCaseName));
        if(vStatus == false) test.fail("Verify line item 'NetPrice'",objGlobal.gErrMsg);
        else test.pass("Verify line item 'NetPrice'","'NetPrice' value is displayed as expected");
        
        clickButton(waitForObject(":salesOrderItem._save_QPushButton"));
        
         }
    
//**************** POsting Invoice through Accounting Tab ***********
         
         this.PostInvoiceInAccounting = function PostInvoiceInAccounting(orderNumber){
             objPerf.startPerf("Transaction Name: Navigate to Accounting|AccountsRecievable|Invoice"); 
                    
                   activateItem(waitForObjectItem(":MainMenuBar", "Accounting"));  
                   activateItem(waitForObjectItem(":MenuItem", "Accounts Receivable"));  
                   activateItem(waitForObjectItem(":menu.accnt.ar_QMenu", "Invoice"));  
                   activateItem(waitForObjectItem(":menu.accnt.arinvoices_QMenu", "List Unposted..."));
                   selectinvoice(orderNumber);
                   
                    
                   test.log(orderNumber)
                   
                   sendEvent("QCloseEvent", waitForObject(":unpostedInvoices_unpostedInvoices"));  
                   objPerf.stopPerf("Transaction Name: Navigate to Accounting|AccountsRecievable|Invoice"); 
          return true;
         }
         function selectinvoice(orderNumber){
             
         

         waitForObjectItem(":_list_XTreeWidget_4",orderNumber);
         clickItem(":_list_XTreeWidget_4", orderNumber, 20, 4, 0, Qt.LeftButton);
         openItemContextMenu(waitForObject(":_list_XTreeWidget_4"), orderNumber,20, 4, 0);

          activateItem(waitForObjectItem(":unpostedInvoices._menu_QMenu", "Post..."));
          clickButton(waitForObject(":getGLDistDate._particular_QRadioButton"));
          clickButton(waitForObject(":getGLDistDate._continue_QPushButton"));
         }
         
          //************** Verify Posted invoice through Customer window*************
             this.verifyInvoiceInCustomer = function verifyInvoiceInCustomer(invoiceNumber ){
          
             
            
             
             clickTab(waitForObject(":customer._tab_QTabWidget_2"), "Accounting");
             verifyInvoice(invoiceNumber);
             clickTab(waitForObject(":invoice view -1._tabWidget_QTabWidget"), "Line Items");
             return true;
             }
            this. VerifyInvoiceLineitems = function VerifyInvoiceLineitems(orderNumber,testCaseName){
                var vStatus = false;
                var objApp = new Application(); 
                var LWidgetName = ":lineItemsTab._invcitem_XTreeWidget";
                 
                 waitForObject(LWidgetName);
                 var obj_QTableWidget = findObject(LWidgetName);
                 var obj_QAbstratItemModel = obj_QTableWidget.model();  
                 var iRowCount = obj_QAbstratItemModel.rowCount();
                  var iColumnCount = obj_QAbstratItemModel.columnCount();
                 
                  var orderIndex = new Array(iRowCount);
                 
                                      
                     for(var i=0;i<iRowCount;i++){
                         j=i+1;
                      var dsOrderDetails = testData.dataset("OrderDetails.tsv");
                   //   var i = testCaseName+(i+1).slice(-1);
                      var OrderNumber = orderNumber+"-00"+j;
                      //var flag=0; 
                                           
                          orderIndex[i] = getTextValue("{column='1' container=':lineItemsTab._invcitem_XTreeWidget' row='"+i+"' type='QModelIndex'}");
                          test.log(orderIndex[i]);
                      if(OrderNumber==orderIndex[i]){
                          test.pass("Order Number is Verified"+orderIndex[i]);
                      }
                      else    test.fail("Order Number is not verified"+orderIndex[i]);
                         
                      
                      vStatus = verifyLineItemValue("{column='2' container=':lineItemsTab._invcitem_XTreeWidget'  row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Product Name")],testCaseName+j));
                      if(vStatus == false) test.fail("Verify line item 'Product Name'",objGlobal.gErrMsg);
                      else test.pass("Verify line item 'Product Name'","'Product Name' value is displayed as expected");
                      
                      vStatus = verifyLineItemValue("{column='5' container=':lineItemsTab._invcitem_XTreeWidget'  row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Product Quantity")],testCaseName+j));
                      if(vStatus == false) test.fail("Verify line item 'Product Quantity'",objGlobal.gErrMsg);
                      else test.pass("Verify line item 'Product Quantity'","'Product Quantity' value is displayed as expected");
                      
                      vStatus = verifyLineItemValue("{column='6' container=':lineItemsTab._invcitem_XTreeWidget'  row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Reserved")],testCaseName+j));
                      if(vStatus == false) test.fail("Verify line item 'Reserved'",objGlobal.gErrMsg);
                      else test.pass("Verify line item 'Reserved'","'Reserved' value is displayed as expected");
                      //test.pass(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Reserved")],testCaseName+j)); 
                      
                      vStatus = verifyLineItemValue("{column='9' container=':lineItemsTab._invcitem_XTreeWidget' row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ProductExtendedPrice")],testCaseName+j));
                      if(vStatus == false) test.fail("Verify line item 'ProductExtendedPrice'",objGlobal.gErrMsg);
                      else test.pass("Verify line item 'ProductExtendedPrice'","'ProductExtendedPrice' value is displayed as expected");
                      
                      vStatus = verifyLineItemValue("{column='8' container=':lineItemsTab._invcitem_XTreeWidget'  row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ProductPrice")],testCaseName+j));
                      if(vStatus == false) test.fail("Verify line item 'ProductPrice'",objGlobal.gErrMsg);
                      else test.pass("Verify line item 'ProductCustPrice'","'ProductPrice' value is displayed as expected");
                
               }
                 
         
             }
             function verifyInvoice(invoiceNumber){

                 waitForObjectItem(":_list_XTreeWidget_3", invoiceNumber);
                 clickItem(":_list_XTreeWidget_3", invoiceNumber, 27, 8, 0, Qt.LeftButton);
                 openItemContextMenu(waitForObject(":_list_XTreeWidget_3"), invoiceNumber,27, 8, 0);

                 activateItem(waitForObjectItem(":customer._menu_QMenu", "View Invoice..."));
                  
                  }
             
              
                 
         
          this.verifyAlltheLineItemValues = function verifyAlltheLineItemValues(testCaseName)
          {
              clickButton(waitForObject(":salesOrderItem._closeBtn"));
              var vStatus = false;
              var objApp = new Application();
              var sWidgetName = ":_lineItemsPage._soitem_XTreeWidget_4";
              waitForObject(sWidgetName);
              var obj_QTableWidget = findObject(sWidgetName);
              var obj_QAbstratItemModel = obj_QTableWidget.model();  
              var iRowCount = obj_QAbstratItemModel.rowCount();
               var iColumnCount = obj_QAbstratItemModel.columnCount();
               
               var i = new Array(iRowCount);
               var j=0;
               for(var i=0;i<iRowCount;i++){
                 j=i+1;
              var dsOrderDetails = testData.dataset("OrderDetails.tsv");
           //   var i = testCaseName+(i+1).slice(-1);
              test.pass(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Product Name")],testCaseName+j));
              vStatus = verifyLineItemValue("{column='2' container=':_lineItemsPage._soitem_XTreeWidget_4' row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Product Name")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'Product Name'",objGlobal.gErrMsg);
              else test.pass("Verify line item 'Product Name'","'Product Name' value is displayed as expected");
              
              vStatus = verifyLineItemValue("{column='10' container=':_lineItemsPage._soitem_XTreeWidget_4' row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Product Quantity")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'Product Quantity'",objGlobal.gErrMsg);
              else test.pass("Verify line item 'Product Quantity'","'Product Quantity' value is displayed as expected");
              
              vStatus = verifyLineItemValue("{column='16' container=':_lineItemsPage._soitem_XTreeWidget_4' row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ProductPrice")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'ProductPrice'",objGlobal.gErrMsg);
              else test.pass("Verify line item 'ProductPrice'","'ProductPrice' value is displayed as expected");
              
              vStatus = verifyLineItemValue("{column='17' container=':_lineItemsPage._soitem_XTreeWidget_4' row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ProductExtendedPrice")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'ProductExtendedPrice'",objGlobal.gErrMsg);
              else test.pass("Verify line item 'ProductExtendedPrice'","'ProductExtendedPrice' value is displayed as expected");
              
              vStatus = verifyLineItemValue("{column='18' container=':_lineItemsPage._soitem_XTreeWidget_4' row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ProductCustPrice")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'ProductCustPrice'",objGlobal.gErrMsg);
              else test.pass("Verify line item 'ProductCustPrice'","'ProductCustPrice' value is displayed as expected");
              test.pass(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Discount Percentage")],testCaseName+j));          
             
              vStatus = verifyLineItemValue("{column='19' container=':_lineItemsPage._soitem_XTreeWidget_4' row='"+i+"' type='QModelIndex'}","N/A");           
              if(vStatus == false) test.fail("Verify line item 'Cust Discount ' is having defect ",objGlobal.gErrMsg);         
              else test.pass("Verify line item 'Discount Percentage'","'Cust Discount Percentage' value is displayed as expected")
              
//              vStatus = verifyLineItemValue("{column='27' container=':_lineItemsPage._soitem_XTreeWidget_4' row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Reserved")],testCaseName+j));
//              if(vStatus == false) test.fail("Verify line item 'Reserved'",objGlobal.gErrMsg);
//              else test.pass("Verify line item 'Reserved'","'Reserved' value is displayed as expected");
              
              vStatus = verifyLineItemValue(":_lineItemsPageSubTotal1",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("SubTotal")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'SubTotal'",objGlobal.gErrMsg);
              else test.pass("Verify line item 'SubTotal'","'SubTotal' value is displayed as expected");
              
              test.pass(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Total")],testCaseName+j));
              vStatus = verifyLineItemValue(":_lineItemsPageTotal1",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Total")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'Total'",objGlobal.gErrMsg);
              else test.pass("Verify line item 'Total'","'Total' value is displayed as expected");
              
              test.pass(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Tax")],testCaseName+j));
              vStatus = verifyLineItemValue(":_lineItemsPageTax1",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Tax")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'Tax'",objGlobal.gErrMsg);
              else test.pass("Verify line item 'Tax'","'Tax' value is displayed as expected");
              
              test.pass(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("DiscountTwoZeros")],testCaseName+j));
              vStatus = verifyLineItemValue("{column='21' container=':_lineItemsPage._soitem_XTreeWidget_4' row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("DiscountTwoZeros")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'List Discount ' is having defect ",objGlobal.gErrMsg);
              else test.pass("Verify line item 'Discount Percentage'","'List Discount Percentage' value is displayed as expected");
              
              test.pass(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ListPrice")],testCaseName+j));
              vStatus = verifyLineItemValue("{column='20' container=':_lineItemsPage._soitem_XTreeWidget_4' row='"+i+"' type='QModelIndex'}",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ListPrice")],testCaseName+j));
              if(vStatus == false) test.fail("Verify line item 'List price' is having defect ",objGlobal.gErrMsg);
              else test.pass("Verify line item 'List price'","'List price' value is displayed as expected");
             /*test.pass(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ListPrice")],testCaseName+(i+1)));
              vStatus = verifyLineItemValue(":_soitem_QModelIndex",testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ListPrice")],testCaseName+(i+1)));
              if(vStatus == false) test.fail("Verify line item 'List price' is having defect ",objGlobal.gErrMsg);
              else test.pass("Verify line item 'Discount Percentage'","'Discount Percentage' value is displayed as expected");*/
          
               }  
          }

    this.saveAndShipOnOrderPage = function saveAndShipOnOrderPage()          
   {            
      var orderNumber=getTextValue(":salesOrder new._orderNumber_XLineEdit");          
      clickButton(waitForObject(":salesOrder new._saveAndAdd_QPushButton"));           
       clickButton(waitForObject(":salesOrder new._close_QPushButton"));            
    // clickButton(waitForObject(":customer._close_QPushButton"));          
      // clickButton(waitForObject(":customers._closeBtn_QToolButton"));          
       return orderNumber;          
    }
    
    
    
   this.getPropertyValue = function getPropertyValue(objLineItem, property)
   {
       
       try
       {
           var chkElement = waitForObject(objLineItem);
           var Prp = object.properties(chkElement);
           var bValue = Prp[property];
           return bValue;
       }
       catch(Exception)
       {
           return null;
       }
       
   }
   function getTextValue(objLineItem)   
   {            
                 
     try          
        {            
         obj = waitForObjectExists(objLineItem);          
         return obj.text;         
       }            
        catch(Exception)         
       {            
            return false;            
       }            
                  
   }            

}
