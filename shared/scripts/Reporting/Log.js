source(findFile("scripts", "Libraries/Global.js"))

function Log()
{
    var objGlobal = new GlobalVariables();
    
    var objXML;
    var sXMLFilePath;
    
    this.createNewXML = function createNewXML()
    {
        var dtCurrentDate = new Date();
        
        var dtYear = dtCurrentDate.getFullYear();
        var dtMonth = dtCurrentDate.getMonth();
        var dtDay = dtCurrentDate.getDate(); 
        
        objXML = new ActiveXObject("Msxml2.DOMDocument.6.0");
        objXML.async = false;
        objXML.validateOnParse = false;
        objXML.resolveExternals = false;
        
        
        var objNode = objXML.createProcessingInstruction("xml","version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"");
        objXML.appendChild(objNode);
        objNode = null;
        objNode = dom.createProcessingInstruction("xml-stylesheet","type=\"text/xsl\" href=\"Result.xsl\"");
        objXML.appendChild(objNode);
        objNode = null;
        
        //Start Time Node
        var objRoot = objXML.createElement("TestSuite");
        var objAttribute = objXML.createAttribute("StartTime");
        objAttribute.value = "";
        objRoot.setAttributeNode(objAttribute);
        
        //End Time node
        objAttribute = objXML.createAttribute("EndTime");
        objAttribute.value = "";
        objRoot.setAttributeNode(objAttribute);
        
        //TP Node
        objAttribute = objXML.createAttribute("TP");
        objAttribute.value = "";
        objRoot.setAttributeNode(objAttribute);
        
        //TF Node
        objAttribute = objXML.createAttribute("TP");
        objAttribute.value = "";
        objRoot.setAttributeNode(objAttribute);
        
        //TotalTestCases Node
        objAttribute = objXML.createAttribute("TotalTestCases");
        objAttribute.value = "";
        objRoot.setAttributeNode(objAttribute);
        
        var sResultsFolderPath = objGlobal.ResultsFolderPath + "\\" + objGlobal.SuiteName + "_Results_" + dtMonth + "-" + dtDay + "-" + dtYear; 
        
        var objFSO = new ActiveXObject("Scripting.FileSystemObject");
        objFSO.CreateFolder(sResultsFolderPath);
        
        //XML_Results_Feb-10-2017_19-40-20
        
        var sXMLFileName = "XML_Results_" + dtMonth + "-" + dtDay + "-" + dtYear + "_" + dtCurrentDate.getHours() + "-" + dtCurrentDate.getMinutes() + "-" + dtCurrentDate.getSeconds() + ".xml";
        sXMLFilePath = sResultsFolderPath + "\\" + sXMLFileName;
        objXML.save(sXMLFilePath);
    }
}